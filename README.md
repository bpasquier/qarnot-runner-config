# Qarnot-Runner-config

Just add your credentials into the start.sh script. And execute the script.

Or use this one line command :
```
bash <(wget -qO- https://salsa.debian.org/bpasquier/qarnot-runner-config/-/raw/main/easystart.sh?inline=false)

```


# Links

- <a href="https://hub.docker.com/r/qarnotlab/gitlab-runner-launcher">Docker Page</a>
- <a href="https://qarnot.com/qalway">Qarnot's website</a> 
- <a href="https://blog.qarnot.com/gitlab-ci-cd-on-qarnot-cloud-documentation/">Runner Documentation</a>
