#!/bin/sh

# Credentials
read -p $'\e[33mEnter your Qarnot Email :\e[0m ' Qarnot_Email
read -p $'\e[34mEnter your Qarnot token :\e[0m ' Qarnot_Token
read -p $'\e[35mEnter the server url :\e[0m ' Server_URL
read -p $'\e[36mEnter your repo token:\e[0m ' Repository_Runner_token

# Create Folders
mkdir config
mkdir runner-logs
touch credentials.txt

#  Keep the credentials into credentials.txt
echo "Qarnot_Email=$Qarnot_Email"  >> credentials.txt
echo "Qarnot_token=$Qarnot_Token" >> credentials.txt
echo "Server_URL=$Server_URL" >> credentials.txt
echo "Registration_Token=$Repository_Runner_token" >> credentials.txt


# Download default configuration file
wget https://salsa.debian.org/bpasquier/qarnot-runner-config/-/raw/main/config/config.default.toml -P config

# Container Commands
podman run --name Runner -dt \
	-e EMAIL="$Qarnot_Email" \
	-e QARNOT_TOKEN="$Qarnot_Token" \
	-e CI_SERVER_URL="$Server_URL" \
	-e RUNNER_NAME="QarnotRunner" \
	-e REGISTRATION_TOKEN="$Repository_Runner_token" \
	-e RUNNER_DEBUG="true" \
	-v "$PWD"/config:/config \
	-v "$PWD"/runner-logs:/runner-logs \
	docker.io/qarnotlab/gitlab-runner-launcher:dmach_v0.16.2-gitlab.18-gitrun_v15.7.2


