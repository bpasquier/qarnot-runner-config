#!/bin/sh

# Credentials
Qarnot_Email=""
Qarnot_Token=""
Server_URL=""
Repository_Runner_token=""

# Container Commands
podman run --name Runner -dt \
	-e EMAIL="$Qarnot_Email" \
	-e QARNOT_TOKEN="$Qarnot_Token" \
	-e CI_SERVER_URL="$Server_URL" \
	-e RUNNER_NAME="QarnotRunner" \
	-e REGISTRATION_TOKEN="$Repository_Runner_token" \
	-e RUNNER_DEBUG="true" \
	-v "$PWD"/config:/config \
	-v "$PWD"/runner-logs:/runner-logs \
	docker.io/qarnotlab/gitlab-runner-launcher:dmach_v0.16.2-gitlab.18-gitrun_v15.7.2

